/* eslint-disable no-param-reassign,no-undef,max-len */
import qs from 'qs';
import { Credentials } from '@storage';

const baseUrl = 'https://practical-day-187807.appspot.com';

// noinspection JSAnnotator
const clientConfig = {
  baseURL: baseUrl,
  responseType: 'json',
 // transformRequest: data => qs.stringify(data),
  paramsSerializer: params => qs.stringify(params),
  headers: {
  //  authorization: 'Basic cHJvamVjdGFwcDpteS1zZWNyZXQtdG9rZW4tdG8tY2hhbmdlLWluLXByb2R1Y3Rpb24=',
  //  'Content-Type': 'application/x-www-form-urlencoded',
  },
};

export default clientConfig;
