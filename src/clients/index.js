import backend from './backendClient';
import token from './tokenClient';

const clients = {
  default: backend,
  token,
};

export default clients;
