import axios from 'axios';
import qs from 'qs';
import clientConfig from './clientConfig';

const backendClient = {
  client: axios.create({
    baseURL: clientConfig.baseURL,
    responseType: 'json',
    transformRequest: data => qs.stringify(data),
    paramsSerializer: params => qs.stringify(params),
    headers: {
      authorization: 'Basic cHJvamVjdGFwcDpteS1zZWNyZXQtdG9rZW4tdG8tY2hhbmdlLWluLXByb2R1Y3Rpb24=',
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  }),
};

export default backendClient;
