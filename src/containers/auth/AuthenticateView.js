/**
 * Authenticate Screen
 *  - Entry screen for all authentication
 *  - User can tap to login, forget password, signup...
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
// import FCM from 'react-native-fcm';
import {
  View,
  Image,
  StyleSheet,
} from 'react-native';
// import axios from 'axios';
import { Actions } from 'react-native-router-flux';

// Consts and Libs
import { AppStyles, AppSizes, AppColors } from '@theme/';

// Components
import { Spacer, Text, Button } from '@ui/';

/* Styles ==================================================================== */
const styles = StyleSheet.create({
  background: {
    backgroundColor: '#ffffff',
    height: AppSizes.screen.height,
    width: AppSizes.screen.width,
    justifyContent: 'center',
  },
  logo: {
    width: AppSizes.screen.width * 0.85,
    height: 200,
    resizeMode: 'contain',
    tintColor: AppColors.textPrimary,
  },
  text: {
    color: AppColors.textPrimary,
    textAlign: 'center',
  },
});

/* Component ==================================================================== */
class Authenticate extends Component {
  static componentName = 'Authenticate';

  sendToken = () => {
    // FCM.getFCMToken().then((notificationToken) => {
    //   alert(notificationToken);
    //   // const requestData = {
    //   //   method: 'get',
    //   //   url: 'https://api.vk.com/method/users.get',
    //   //   params: {
    //   //     v: vkApiVersion,
    //   //     access_token: token,
    //   //     fields: vkUserFields.join(','),
    //   //   },
    //   // };
    //   // axios(requestData).then(() => {
    //   //   alert('Success');
    //   // }, () => {
    //   //   alert('Error with send token');
    //   // })
    // }, () => {
    //   alert('Error with get token');
    // });
  }

  render = () => (
    <View style={[AppStyles.container, AppStyles.containerCentered, styles.background]}>
      <Image
        source={require('../../images/logo-black.png')}
        style={[styles.logo]}
      />
      <View style={{ width: 200 }}>
        <Text style={[styles.text]}>Личный кабинет участника медецинских исследований</Text>
      </View>

      <Spacer size={80} />

      <View style={[AppStyles.row, AppStyles.paddingHorizontal]}>
        <View style={[{ width: 200 }]}>
          <Button
            title={'Войти'}
            icon={{ name: 'lock' }}
            onPress={Actions.login}
          />
        </View>
      </View>

      <Spacer size={80} />
    </View>
  )
}

/* Export Component ==================================================================== */
export default Authenticate;
