/**
 * Research Tabs Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import { connect } from 'react-redux';

// The component we're mapping to
import ResearchTabsRender from './BrowseView';

// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  types: state.history.types || [],
});

// Any actions to map to the component?
const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(ResearchTabsRender);
