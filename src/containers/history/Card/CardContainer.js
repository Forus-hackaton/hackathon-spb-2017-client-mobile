/**
 * Individual Research Card Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

// Actions
import * as ResearchActions from '@redux/researches/actions';

// Components
import ResearchCardRender from './CardView';

/* Redux ==================================================================== */
// What data from the store shall we send to the component?
const mapStateToProps = state => ({
});

// Any actions to map to the component?
const mapDispatchToProps = {
};

/* Component ==================================================================== */
class ResearchCard extends Component {
  static componentName = 'ResearchCard';

  static propTypes = {
    historyItem: PropTypes.object.isRequired,
  }

  static defaultProps = {
  }

  constructor(props) {
    super(props);
    this.state = { historyItem: props.historyItem };
  }

  componentWillReceiveProps(props) {
    if (props.historyItem) {
      this.setState({ historyItem: props.historyItem });
    }
  }

  /**
    * On Press of Card
    */
  onPressCard = () => {
    Actions.researchView({
      title: this.props.historyItem.name,
      research: this.props.historyItem,
    });
  }

  render = () => {
    const { historyItem } = this.state;
    return (
      <ResearchCardRender
        historyItem={historyItem}
        onPress={this.onPressCard}
      />
    );
  }
}

/* Export Component ==================================================================== */
export default connect(mapStateToProps, mapDispatchToProps)(ResearchCard);
