/**
 * Recipe View Screen
 *  - The individual recipe screen
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  View,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import { Icon } from 'react-native-elements';
import moment from 'moment';

// Consts and Libs
import { AppStyles } from '@theme/';

// Components
import { Card, Text } from '@ui/';

/* Styles ==================================================================== */
const styles = StyleSheet.create({
});

/* Component ==================================================================== */
class ResearchCard extends Component {
  static componentName = 'ResearchCard';

  static propTypes = {
    historyItem: PropTypes.object.isRequired,
    onPress: PropTypes.func,
  }

  static defaultProps = {
    onPress: null,
    onPressFavourite: null,
    isFavourite: null,
  }

  render = () => {
    const { historyItem, onPress } = this.props;

    const date = moment(historyItem.date);

    return (
      <TouchableOpacity activeOpacity={0.8} onPress={onPress}>
        <Card>
          <View style={[AppStyles.paddingBottomSml]}>
            <Text h3>{historyItem.name}</Text>
            <View>
              <Text>Начало: {date.calendar()}</Text>
            </View>
          </View>
        </Card>
      </TouchableOpacity>
    );
  }
}

/* Export Component ==================================================================== */
export default ResearchCard;
