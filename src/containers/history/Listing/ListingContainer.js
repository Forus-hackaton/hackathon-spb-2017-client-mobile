/**
 * List of Recipes for a Meal Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Actions
import * as ResearchActions from '@redux/history/actions';

// Components
import Loading from '@components/general/Loading';
import ResearchListingRender from './ListingView';

/* Redux ==================================================================== */
// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  historyItems: state.history.historyItems || [],
});

// Any actions to map to the component?
const mapDispatchToProps = {
};

/* Component ==================================================================== */
class TypeListing extends Component {
  static componentName = 'TypeListing';

  static propTypes = {
    historyItems: PropTypes.arrayOf(PropTypes.object),
    type: PropTypes.string.isRequired,
  }

  static defaultProps = {
    historyItems: [],
  }

  state = {
    loading: false,
    historyItems: [],
  }

  componentDidMount = () => this.getThisTypesResearches(this.props.historyItems);
  componentWillReceiveProps = props => this.getThisTypesResearches(props.historyItems);

  /**
    * Pick out researches that are in the current type
    * And hide loading state
    */
  getThisTypesResearches = (allResearches) => {
    if (allResearches.length > 0) {
      const historyItems = allResearches.filter(research =>
        this.props.type === 'all' || research.type === this.props.type,
      );

      this.setState({
        historyItems,
        loading: false,
      });
    }
  }

  render = () => {
    if (this.state.loading) return <Loading />;

    return (
      <ResearchListingRender
        historyItems={this.state.historyItems}
        reFetch={this.fetchResearches}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TypeListing);
