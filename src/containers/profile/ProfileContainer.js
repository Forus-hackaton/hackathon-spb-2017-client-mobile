import { connect } from 'react-redux';

// The component we're mapping to
import ProfileView from './ProfileView';

// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  diseases: state.medicalRecord.diseases,
  allergens: state.medicalRecord.allergens,
});

// Any actions to map to the component?
const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(ProfileView);
