import React, { Component } from 'react';
import {
  View,
  ListView,
  ScrollView,
  StyleSheet,
} from 'react-native';
import { Actions } from 'react-native-router-flux';

import { AppStyles, AppSizes } from '@theme/';
import {
  Alerts,
  Button,
  Card,
  Spacer,
  Text,
  List,
  ListItem,
  FormInput,
  FormLabel,
} from '@ui/';

const styles = StyleSheet.create({
});

class ProfileView extends Component {
  static componentName = 'ProfileView';

  static propTypes = {
  }

  static defaultProps = {
  }

  constructor(props) {
    super(props);

    const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });
    const ds2 = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

    this.state = {
      diseasesDS: ds.cloneWithRows(props.diseases),
      allergensDS: ds2.cloneWithRows(props.allergens),
    };
  }

  renderRow = (data, sectionID) => (
    <ListItem
      key={`list-row-${sectionID}`}
      onPress={Actions.comingSoon}
      title={data.title}
      subtitle={data.role || null}
      leftIcon={data.icon ? { name: data.icon } : null}
      avatar={data.avatar ? { uri: data.avatar } : null}
      roundAvatar={!!data.avatar}
    />
  )

  render = () => {
    return (
      <View>
        <ScrollView
          automaticallyAdjustContentInsets={false}
          style={[AppStyles.container, { height: 500 }]}
        >
          <View style={[AppStyles.paddingHorizontal]}>
            <Spacer size={15} />
            <Text h2>Обнарудженные заболевания</Text>
            <Spacer size={-10} />
          </View>
          <List>
            <ListView
              renderRow={this.renderRow}
              dataSource={this.state.diseasesDS}
            />
          </List>
        </ScrollView>
      </View>
    );
  }
}

/* Export Component ==================================================================== */
export default ProfileView;
