/**
 * Individual Research Card Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';

// Actions
import * as ResearchActions from '@redux/researches/actions';

// Components
import ResearchCardRender from './CardView';

/* Redux ==================================================================== */
// What data from the store shall we send to the component?
const mapStateToProps = state => ({
});

// Any actions to map to the component?
const mapDispatchToProps = {
};

/* Component ==================================================================== */
class ResearchCard extends Component {
  static componentName = 'ResearchCard';

  static propTypes = {
    research: PropTypes.object.isRequired,
  }

  static defaultProps = {
  }

  constructor(props) {
    super(props);
    this.state = { research: props.research };
  }

  componentWillReceiveProps(props) {
    if (props.research) {
      this.setState({ research: props.research });
    }
  }

  /**
    * On Press of Card
    */
  onPressCard = () => {
    Actions.researchView({
      title: this.props.research.name,
      research: this.props.research,
    });
  }

  render = () => {
    const { research } = this.state;
    return (
      <ResearchCardRender
        research={research}
        onPress={this.onPressCard}
      />
    );
  }
}

/* Export Component ==================================================================== */
export default connect(mapStateToProps, mapDispatchToProps)(ResearchCard);
