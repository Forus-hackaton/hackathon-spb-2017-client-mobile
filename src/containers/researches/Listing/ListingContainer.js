/**
 * List of Recipes for a Meal Container
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

// Actions
import * as ResearchActions from '@redux/researches/actions';

// Components
import Loading from '@components/general/Loading';
import ResearchListingRender from './ListingView';

/* Redux ==================================================================== */
// What data from the store shall we send to the component?
const mapStateToProps = state => ({
  researches: state.research.researches || [],
});

// Any actions to map to the component?
const mapDispatchToProps = {
  getResearches: ResearchActions.getResearches,
};

/* Component ==================================================================== */
class TypeListing extends Component {
  static componentName = 'TypeListing';

  static propTypes = {
    researches: PropTypes.arrayOf(PropTypes.object),
    type: PropTypes.string.isRequired,
    getResearches: PropTypes.func.isRequired,
  }

  static defaultProps = {
    researches: [],
  }

  state = {
    loading: false,
    researches: [],
  }

  componentDidMount = () => this.getThisTypesResearches(this.props.researches);
  componentWillReceiveProps = props => this.getThisTypesResearches(props.researches);

  /**
    * Pick out researches that are in the current type
    * And hide loading state
    */
  getThisTypesResearches = (allResearches) => {
    if (allResearches.length > 0) {
      const researches = allResearches.filter(research =>
        research.type.toString() === this.props.type.toString(),
      );

      this.setState({
        researches,
        loading: false,
      });
    }
  }

  /**
    * Fetch Data from API
    */
  fetchResearches = () => this.props.getResearches()
    .then(() => this.setState({ error: null, loading: false }))
    .catch(err => this.setState({ error: err.message, loading: false }))

  render = () => {
    if (this.state.loading) return <Loading />;

    return (
      <ResearchListingRender
        researches={this.state.researches}
        reFetch={this.fetchResearches}
      />
    );
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(TypeListing);
