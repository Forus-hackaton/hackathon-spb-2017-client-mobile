/**
 * Tabs Scenes
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import React from 'react';
import { Scene } from 'react-native-router-flux';

// Consts and Libs
import { AppConfig } from '@constants/';
import { AppStyles, AppSizes } from '@theme/';

// Components
import { TabIcon } from '@ui/';
import { NavbarMenuButton } from '@containers/ui/NavbarMenuButton/NavbarMenuButtonContainer';

// Scenes
// import Placeholder from '@components/general/Placeholder';
// import Error from '@components/general/Error';
// import StyleGuide from '@containers/StyleGuideView';
// import Recipes from '@containers/recipes/Browse/BrowseContainer';
// import RecipeView from '@containers/recipes/RecipeView';
import Researches from '@containers/researches/Browse/BrowseContainer';
import ResearchView from '@containers/researches/ResearchView';
import HistoryListing from '@containers/history/Browse/BrowseContainer';
import HistoryItemView from '@containers/history/HistoryItemView';
import ProfileContainer from '@containers/profile/ProfileContainer';

const navbarPropsTabs = {
  ...AppConfig.navbarProps,
  renderLeftButton: () => <NavbarMenuButton />,
  sceneStyle: {
    ...AppConfig.navbarProps.sceneStyle,
    paddingBottom: AppSizes.tabbarHeight,
  },
};

/* Routes ==================================================================== */
const scenes = (
  <Scene key={'tabBar'} tabs tabBarIconContainerStyle={AppStyles.tabbar} pressOpacity={0.95}>

    <Scene
      {...navbarPropsTabs}
      key={'history'}
      title={'История'}
      icon={props => TabIcon({ ...props, icon: 'search' })}
    >
      <Scene
        {...navbarPropsTabs}
        key={'historyListing'}
        component={HistoryListing}
        title={'История'}
        analyticsDesc={'History: Browse History'}
      />
      <Scene
        {...AppConfig.navbarProps}
        key={'historyView'}
        component={HistoryItemView}
        getTitle={props => ((props.name) ? props.name : 'Запись')}
        analyticsDesc={'HistoryView: View History Item'}
      />
    </Scene>
    
    <Scene
      {...navbarPropsTabs}
      key={'researches'}
      title={'Исследования'}
      icon={props => TabIcon({ ...props, icon: 'search' })}
    >
      <Scene
        {...navbarPropsTabs}
        key={'recipesListing'}
        component={Researches}
        title={'Исследования'}
        analyticsDesc={'Recipes: Browse Recipes'}
      />
      <Scene
        {...AppConfig.navbarProps}
        key={'recipeView'}
        component={ResearchView}
        getTitle={props => ((props.name) ? props.name : 'Исследование')}
        analyticsDesc={'RecipeView: View Recipe'}
      />
    </Scene>


    {/* <Scene
      {...navbarPropsTabs}
      key={'recipes'}
      title={'Recipes'}
      icon={props => TabIcon({ ...props, icon: 'search' })}
    >
      <Scene
        {...navbarPropsTabs}
        key={'recipesListing'}
        component={Recipes}
        title={'Recipes'}
        analyticsDesc={'Recipes: Browse Recipes'}
      />
      <Scene
        {...AppConfig.navbarProps}
        key={'recipeView'}
        component={RecipeView}
        getTitle={props => ((props.title) ? props.title : 'View Recipe')}
        analyticsDesc={'RecipeView: View Recipe'}
      />
    </Scene>

    <Scene
      key={'timeline'}
      {...navbarPropsTabs}
      title={'Coming Soon'}
      component={Placeholder}
      icon={props => TabIcon({ ...props, icon: 'timeline' })}
      analyticsDesc={'Placeholder: Coming Soon'}
    />

    <Scene
      key={'error'}
      {...navbarPropsTabs}
      title={'Example Error'}
      component={Error}
      icon={props => TabIcon({ ...props, icon: 'error' })}
      analyticsDesc={'Error: Example Error'}
    />

    <Scene
      key={'styleGuide'}
      {...navbarPropsTabs}
      title={'Style Guide'}
      component={StyleGuide}
      icon={props => TabIcon({ ...props, icon: 'speaker-notes' })}
      analyticsDesc={'StyleGuide: Style Guide'}
    />
    */}


    <Scene
      {...navbarPropsTabs}
      key={'profile'}
      title={'Медкарта'}
      component={ProfileContainer}
      icon={props => TabIcon({ ...props, icon: 'speaker-notes' })}
      analyticsDesc={'Profile'}
    />
  </Scene>
);

export default scenes;
