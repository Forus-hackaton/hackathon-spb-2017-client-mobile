/**
 * Recipe Default Store
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
export default {
  types: [
    {
      type: 'all',
      title: 'Все',
    },
    {
      type: 'drug',
      title: 'Рецепты',
    },
    {
      type: 'therapy',
      title: 'Лечения',
    },
    {
      type: 'analysis',
      title: 'Анализы',
    },
  ],
  historyItems: [
    {
      id: 1,
      type: 'analysis',
      name: 'Анализ крови на гепатит А',
      date: '2017-01-20 22:00:00',
    },
    {
      id: 2,
      type: 'therapy',
      name: 'Внутривенная Химиотерапия',
      date: '2017-03-23 22:00:00',
    },
    {
      id: 3,
      type: 'drug',
      name: 'Оцилококцинум',
      date: '2017-03-23 22:00:00',
    },
  ],
};
