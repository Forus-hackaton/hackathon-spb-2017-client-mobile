export default {
  diseases: [{
    id: 1,
    code: '2315-435',
    title: 'Туберкулез',
  }, {
    id: 2,
    code: '2553-234',
    title: 'Хроническая обструктивная болезнь легких',
  }, {
    id: 3,
    code: '6453-242',
    title: 'Малярия',
  }, {
    id: 4,
    code: '7897-231',
    title: 'Рак легких, трахеи и бронхов',
  }, {
    id: 5,
    code: '2563-224',
    title: 'Стоматит',
  }],
  allergens: [{
    id: 1,
    name: 'Бета-лактамные антибиотики',
  }, {
    id: 2,
    name: 'Арахис',
  }, {
    id: 3,
    name: 'Сульфаниламиды',
  }],
};
