/**
 * Recipe Actions
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import { Firebase, FirebaseRef } from '@constants/';

/**
  * Get Meals
  */
export function getTypes() {
  if (Firebase === null) return () => new Promise(resolve => resolve());

  return dispatch => new Firebase.Promise((resolve) => {
    const ref = FirebaseRef.child('types');

    return ref.once('value').then((snapshot) => {
      const types = snapshot.val() || {};

      return resolve(dispatch({
        type: 'researches/TYPES_REPLACE',
        data: types,
      }));
    });
  });
}

/**
  * Get Recipes
  */
export function getResearches() {
  if (Firebase === null) return () => new Promise(resolve => resolve());

  return dispatch => new Firebase.Promise((resolve) => {
    const ref = FirebaseRef.child('researches');

    return ref.on('value', (snapshot) => {
      const researches = snapshot.val() || {};

      return resolve(dispatch({
        type: 'researches/RESEARCHES_REPLACE',
        data: researches,
      }));
    });
  });
}
