/**
 * Research Reducer
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
import Store from './store';

// Set initial state
export const initialState = Store;

export default function researchReducer(state = initialState, action) {
  switch (action.type) {
    case 'researches/TYPES_REPLACE': {
      return {
        ...state,
        meals: action.data,
      };
    }
    case 'researches/RESEARCHES_REPLACE': {
      let researches = [];

      // Pick out the props I need
      if (action.data && typeof action.data === 'object') {
        researches = action.data;
      }

      return {
        ...state,
        researches,
      };
    }
    default:
      return state;
  }
}
