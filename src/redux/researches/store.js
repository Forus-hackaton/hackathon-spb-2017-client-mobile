/**
 * Recipe Default Store
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */
export default {
  types: [
    { id: 1, title: 'Активные' },
    { id: 2, title: 'Прошедшие' },
  ],
  researches: [
    {
      id: 1,
      type: 1,
      name: 'Исследования влияния вейпов на нормальных людей',
      dateFrom: '2017-12-03 22:00:00',
      dateTo: '2018-01-20 22:00:00',
    },
    {
      id: 2,
      type: 1,
      name: 'Испытание эксперементального препарата против синдрома Хана Батыя',
      dateFrom: '2017-12-23 22:00:00',
      dateTo: '2018-01-25 22:00:00',
    },
    {
      id: 2,
      type: 2,
      name: 'Испытание эксперементального препарата против синдрома Хана Батыя',
      dateFrom: '2017-11-03 22:00:00',
      dateTo: '2017-12-02 22:00:00',
    },
  ],
};
