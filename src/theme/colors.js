/**
 * App Theme - Colors
 *
 * React Native Starter App
 * https://github.com/mcnamee/react-native-starter-app
 */

const app = {
  background: '#ffffff',
  cardBackground: '#fbfcff',
  listItemBackground: '#FFFFFF',
};

const brand = {
  brand: {
    primary: '#ff2d55',
    secondary: '#567b9a',
  },
};

const text = {
  textPrimary: '#25537a',
  textSecondary: '#777777',
  headingPrimary: brand.brand.primary,
  headingSecondary: brand.brand.primary,
};

const borders = {
  border: '#efeff5',
};

const tabbar = {
  tabbar: {
    background: '#ffffff',
    iconDefault: '#BABDC2',
    iconSelected: brand.brand.primary,
  },
};

export default {
  ...app,
  ...brand,
  ...text,
  ...borders,
  ...tabbar,
};
